import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

import {Calcul} from "../../models/calcul";
import {Formation} from "../../models/formation_model";

@Injectable()
export class DataServiceProvider {

	Calculs: Calcul[];
	Formations: Formation[];

	date =  new Date();
	datesort = new Date();
	
  constructor(public datepipe: DatePipe) {
    
  }

  getDate(datepar){

     var dateParts = datepar.split("-");
     var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
     return date;
  }

  loopsCalcul(date: Date, jour: number)
  {
  	this.datesort =  new Date(date.getFullYear(), date.getMonth()+1, date.getDate()+jour);
  	return this.datepipe.transform(this.datesort, "dd MMMM yyyy","", "fr-CA");
  }


  getCalculgrossesse(date)
  {
  	this.date = this.getDate(date);
  	console.log(this.date.getMonth());
  	return this.Calculs = 
	[
	  	{
	  		text:"Date prévue de votre accouchement",
	  		date: ""+this.loopsCalcul(this.date, 282)
	  	},

	  	{
	  		text:"Date après laquelle votre enfant n'est plus prématuré",
	  		date: ""+this.loopsCalcul(this.date, 258)
	  	},

	  	{
	  		text:"Inscription dans une maternité",
	  		date:""+this.loopsCalcul(this.date, 44 )
	  	},

	  	{
	  		text:"Date limite pour votre première consultation, votre premier bilan sanguin et votre déclaration de grossesse",
	  		date: ""+this.loopsCalcul(this.date, 98 )
	  	},

	  	{
	  		text:"Ponction trophoblaste",
	  		date:""+this.loopsCalcul(this.date, 77 )
	  	},

	  	{
	  		text:"Inscription dans une maternité",
	  		date:""+this.loopsCalcul(this.date, 44 )
	  	},

	  	{
	  		text:"Dépistage de la trisomie 21",
	  		date:""+this.loopsCalcul(this.date, 98 )+" jusqu'au "+""+this.loopsCalcul(this.date, 126)
	  	},

	  	{
	  		text:"Amniocentèse",
	  		date:""+this.loopsCalcul(this.date, 98 )+" jusqu'au "+""+this.loopsCalcul(this.date, 112)
	  	},

	  	{
	  		text:"Examen bucco-dentaire (conseillé)",
	  		date:""+this.loopsCalcul(this.date, 105 )+" jusqu'au "+""+this.loopsCalcul(this.date, 135)
	  	},
	  	{
	  		text:"Deuxième échographie",
	  		date:""+this.loopsCalcul(this.date, 140 )+" jusqu'au "+""+this.loopsCalcul(this.date, 154)
	  	},
	  	{
	  		text:"Bilan sanguin avec sérologie de l'hépatite B",
	  		date:""+this.loopsCalcul(this.date, 167 )+" jusqu'au "+""+this.loopsCalcul(this.date, 197)
	  	},
	  	{
	  		text:"Troisième échographie",
	  		date:""+this.loopsCalcul(this.date, 210 )+" jusqu'au "+""+this.loopsCalcul(this.date, 224)
	  	},
	  	{
	  		text:"2ème détermination du groupe sanguin",
	  		date:""+this.loopsCalcul(this.date, 228 )+" jusqu'au "+""+this.loopsCalcul(this.date, 254)
	  	},
	  	{
	  		text:"Consultation avec l'anesthésiste",
	  		date:""+this.loopsCalcul(this.date, 210 )+" jusqu'au "+""+this.loopsCalcul(this.date, 224)
	  	},

	  	{
	  		text:"Début du congé prénatal pour un premier ou second enfant",
	  		date:""+this.loopsCalcul(this.date, 228 )
	  	},

	  	{
	  		text:"Début du congé prénatal pour un troisième enfant ou plus",
	  		date:""+this.loopsCalcul(this.date, 214 )
	  	},

	  	{
	  		text:"Début du congé prénatal pour des jumeaux",
	  		date:""+this.loopsCalcul(this.date, 200 )
	  	},

	  	{
	  		text:"Début du congé prénatal pathologique pour un premier ou second enfant",
	  		date:""+this.loopsCalcul(this.date, 228 )
	  	},

	  	{
	  		text:"Début du congé prénatal pathologique pour un troisième enfant ou plus",
	  		date:""+this.loopsCalcul(this.date, 214 )
	  	},

	  	{
	  		text:"Début du congé prénatal pathologique pour des jumeaux",
	  		date:""+this.loopsCalcul(this.date, 186 )
	  	},
	  	{
	  		text:"Fin du congé pour un premier ou second enfant",
	  		date:""+this.loopsCalcul(this.date, 354 )
	  	},

	  	{
	  		text:"Fin du congé pour un troisième enfant ou plus",
	  		date:""+this.loopsCalcul(this.date, 410 )
	  	},
	  	{
	  		text:"Fin du congé pour des jumeaux",
	  		date:""+this.loopsCalcul(this.date, 438 )
	  	},
	]
  }


  getFormation()
  {
  	return this.Formations = 
  	[
  		{
  			titre:"Premier trimestre de grossesse",
  			description:"Félicitations, vous êtes enceinte ! c'est ainsi que commence une des plus belles histoires entre un homme et une femme qui ont décidé de mettre un enfant au monde.Le premier trimestre de grossesse est sans aucun doute celui le plus chargé en émotion (avec la naissance bien sûr).",
  			image:"../../assets/imgs/trimestre1.jpg"
  		},

  		{
  			titre:"Deuxième trimestre de grossesse",
  			description:"La métamorphose du corps.Le deuxième trimestre est souvent celui le plus serein pour la future maman et pour son entourage. Les réponses aux questions du premier trimestre ont été données, les angoisses",
  			image:"../../assets/imgs/trimestre2.jpg"
  		},

  		{
  			titre:"Troisième trimestre de grossesse",
  			description:"La préparation à l'accouchement.L'entrée dans le troisième trimestre marque la dernière ligne droite : trois mois séparent les futurs parents de l'arrivée de bébé et le stress commence à faire surface",
  			image:"../../assets/imgs/trimestre3.jpg"
  		},
  	]
  }

  
}
