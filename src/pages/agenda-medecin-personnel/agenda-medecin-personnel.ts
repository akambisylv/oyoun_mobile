import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-agenda-medecin-personnel',
  templateUrl: 'agenda-medecin-personnel.html',
})
export class AgendaMedecinPersonnelPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgendaMedecinPersonnelPage');
  }

}
