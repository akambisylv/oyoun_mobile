import { Component } from '@angular/core';
import {NavController, NavParams, ViewController } from 'ionic-angular';


//page

import {ConnexionPage} from "../connexion/connexion";
import {MenuPage} from "../menu/menu";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {

  constructor(public navCtrl: NavController, 
  			public navParams: NavParams,
  			private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.viewCtrl.showBackButton(false);
  }

  gotoconnexionPage()
  {
  	this.navCtrl.push(ConnexionPage);
  }


  gotopageMenu()
  {
  	this.navCtrl.push(MenuPage);
  }

}
