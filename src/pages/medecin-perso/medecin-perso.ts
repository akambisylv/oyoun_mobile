import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import{TabsMedecinPage} from "../tabs-medecin/tabs-medecin";


@Component({
  selector: 'page-medecin-perso',
  templateUrl: 'medecin-perso.html',
})
export class MedecinPersoPage {

	 isAndroid: boolean = false;
 
  constructor(public navCtrl: NavController, 
  			public navParams: NavParams,
  			public platform: Platform) {
  	this.isAndroid = platform.is('android');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedecinPersoPage');
  }

}
