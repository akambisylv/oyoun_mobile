import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import{MedecinPersoPage} from "../medecin-perso/medecin-perso";
import {AgendaMedecinPersonnelPage} from "../agenda-medecin-personnel/agenda-medecin-personnel";

@Component({
  template: `
    <ion-tabs class="tabs-icon-text" [color]="isAndroid ? 'royal' : 'primary'">
      <ion-tab tabIcon="person" tabTitle="Medecin" [root]="rootPage"></ion-tab>
      <ion-tab tabIcon="calendar" tabTitle="Agenda" [root]="rootPage1"></ion-tab>
      <ion-tab tabIcon="book" tabTitle="Fiche" [root]="rootPage"></ion-tab>
      <ion-tab tabIcon="pulse" tabTitle="Stat" [root]="rootPage"></ion-tab>
    </ion-tabs>
`})
export class TabsMedecinPage {

	rootPage = MedecinPersoPage;
  rootPage1 = AgendaMedecinPersonnelPage;

	isAndroid: boolean = false;

  constructor(public navCtrl: NavController, 
  			public navParams: NavParams,
  			public platform: Platform) {
  	this.isAndroid = platform.is('android');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsMedecinPage');
  }

}
