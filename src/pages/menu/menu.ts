import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, MenuController} from 'ionic-angular';

import {MenuAcceuilPage} from "../menu-acceuil/menu-acceuil";

import {MaGrossessePage} from "../ma-grossesse/ma-grossesse";
import {MedecinPersoPage} from "../medecin-perso/medecin-perso";
import {TabsMedecinPage} from "../tabs-medecin/tabs-medecin";
import {ConnexionPage} from "../connexion/connexion";

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

	private rootPage;
  private MaGrossessePage;
  private MedecinPersoPage;
	private TabsMedecinPage;

  constructor(public navCtrl: NavController, 
  				public navParams: NavParams,
  				private viewCtrl:ViewController,
  				private menu: MenuController) {

  	this.menu.enable(true);
  	this.rootPage = MenuAcceuilPage;
    this.MaGrossessePage = MaGrossessePage;
    this.MedecinPersoPage = MedecinPersoPage;
  	this.TabsMedecinPage = TabsMedecinPage;
  }

  ionViewDidLoad() {
     this.viewCtrl.showBackButton(false);
  }


  openPage(p)
  {
  	this.rootPage = p;
  	this.menu.close();
  }

  logout()
  {
    this.menu.close();
    this.navCtrl.push(ConnexionPage);
  }
  
}
