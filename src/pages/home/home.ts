import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//pages
import {LoginPage} from "../login/login";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(public navCtrl: NavController) {
  }


  openloginPage()
  {
  	this.navCtrl.push(LoginPage).then(() => {
	  let index = this.navCtrl.length()-2;
	  this.navCtrl.remove(index);
	});
  }
	
}
