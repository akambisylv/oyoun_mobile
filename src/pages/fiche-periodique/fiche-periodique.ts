import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-fiche-periodique',
  templateUrl: 'fiche-periodique.html',
})
export class FichePeriodiquePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichePeriodiquePage');
  }

}
