import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';

//page

import {LoginPage} from '../login/login';
import {MenuPage} from "../menu/menu";

@Component({
  selector: 'page-connexion',
  templateUrl: 'connexion.html',
})
export class ConnexionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnexionPage');
  }

  gotoInscriptionPage()
  {
  	this.navCtrl.push(LoginPage);
  }

  gotopageMenu()
  {
  	this.navCtrl.push(MenuPage);
  }

}
