import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import {DataServiceProvider} from "../../providers/data-service/data-service";
import {Calcul} from "../../models/calcul";
@Component({
  selector: 'page-ma-grossesse',
  templateUrl: 'ma-grossesse.html',
})
export class MaGrossessePage {

	
	calcul: Calcul = {text:"", date: "" };
	calculs: Calcul[];

  constructor(public navCtrl: NavController, 
  			public navParams: NavParams,
  			private menu: MenuController,
  			private dataService: DataServiceProvider) {

  	this.menu.enable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaGrossessePage');
    
  }

  functionCalcul()
  {
  	this.calculs = this.dataService.getCalculgrossesse(this.calcul.date.toString());
  }

}
