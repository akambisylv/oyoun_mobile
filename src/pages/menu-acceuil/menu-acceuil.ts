import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import {Formation} from "../../models/formation_model";

import {DataServiceProvider} from '../../providers/data-service/data-service';


@Component({
  selector: 'page-menu-acceuil',
  templateUrl: 'menu-acceuil.html',
})
export class MenuAcceuilPage {

  formation: Formation = {titre:"", description: "", image:"" };
  formations: Formation[];



  constructor(public navCtrl: NavController, 
  			public navParams: NavParams,
  			private menu: MenuController,
        private dataservice: DataServiceProvider) {
  	this.menu.enable(true);
    this.formations = this.dataservice.getFormation();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuAcceuilPage');
    
  }

}
