import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import {registerLocaleData} from '@angular/common/';
import { CalendarModule } from 'ionic3-calendar-en';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {ConnexionPage} from "../pages/connexion/connexion";
import {MenuPage} from "../pages/menu/menu";
import {MenuAcceuilPage} from "../pages/menu-acceuil/menu-acceuil";
import {MaGrossessePage} from "../pages/ma-grossesse/ma-grossesse";
import {MedecinPersoPage} from "../pages/medecin-perso/medecin-perso";
import {TabsMedecinPage} from "../pages/tabs-medecin/tabs-medecin";
import {InfoMedecinPage} from "../pages/info-medecin/info-medecin";

import {AgendaMedecinPersonnelPage} from "../pages/agenda-medecin-personnel/agenda-medecin-personnel";
import {FichePeriodiquePage} from "../pages/fiche-periodique/fiche-periodique";


import { DataServiceProvider } from '../providers/data-service/data-service';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    MenuPage,
    ConnexionPage,
    MenuAcceuilPage,
    MaGrossessePage,
    MedecinPersoPage,
    TabsMedecinPage,
    InfoMedecinPage,
    AgendaMedecinPersonnelPage,
    FichePeriodiquePage,
    HomePage
  ],
  imports: [
    BrowserModule,
    CalendarModule,
    IonicModule.forRoot(MyApp,{
      monthNames: ['Janvier', 'Février', 'Mars','Avril', 'Mai', 
                    'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre',
                    'Novembre', 'Decembre' ],
      monthShortNames: ['jan', 'fev', 'mar','avr', 'mai', 'jui', 'juil', 'aout', 'sep', 'oct', 'nov', 'dec' ],
      dayNames: ['Lundi', 'Mardi', 'Mercredi','jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
      dayShortNames: ['lun', 'mar', 'mer','jeu', 'ven', 'sam', 'dim'],
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    MenuPage,
    ConnexionPage,
    MenuAcceuilPage,
    MaGrossessePage,
    MedecinPersoPage,
    TabsMedecinPage,
    InfoMedecinPage,
    AgendaMedecinPersonnelPage,
    FichePeriodiquePage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler, useValue: 'fr-CA'},
    DataServiceProvider,

  ]
})
export class AppModule {}
