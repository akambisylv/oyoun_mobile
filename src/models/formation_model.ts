export interface Formation{
	titre: string,
	description: string,
	image: string
}